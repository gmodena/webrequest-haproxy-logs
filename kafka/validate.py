import argparse
from kafka import KafkaConsumer
import json
from jsonschema import validate
import logging
import yaml

logging.basicConfig(level=logging.ERROR)

def validate_message(message, schema):
    try:
        validate(instance=message, schema=schema)
    except Exception as e:
        logging.error(f"Message validation failed: {e}")
        return False
    return True

def main(topic):
    with open("schema.yaml", "r") as schema_file:
        schema = yaml.safe_load(schema_file)

    kafka_servers = 'kafka-jumbo1007.eqiad.wmnet:9092'

    consumer = KafkaConsumer(topic,
                             bootstrap_servers=kafka_servers,
                             value_deserializer=lambda x: json.loads(x.decode('utf-8')))

    for message in consumer:
        message_value = message.value
        print(message_value)
        if not validate_message(message_value, schema):
            logging.error(f"Invalid message: {message_value}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Kafka Consumer with JSON schema validation")
    parser.add_argument('topic', type=str, help='The Kafka topic to consume messages from')
    args = parser.parse_args()

    main(args.topic)
