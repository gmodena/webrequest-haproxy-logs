Validate webrequest_frontend topics againt
haproxykafka json schema

Run with:
```
python validate.py <topic_name>
```

For example:
```
python validate.py webrequest_text_test
```

# Dependencies
Requires kafka, jsonchema and yaml library that can be provided
in the top level `conda-environment.yaml` config.
